const express = require('express');
const fs = require('fs');
const morgan = require('morgan');
const app = express();
const port = 8080;



app.use(express.json());
app.use(morgan("tiny"));
app.post('/api/files', (req, res) => {
    let filename = req.body.filename;

    if (!filename.match(/\.xml$|\.txt$|\.json|\.js|\.html|\.log|\.yaml$/gm)) {
        res.status(400).send({"message": "Please specify 'extension' parameter"});
        return;
    }
    if (fs.existsSync(`./api/files/${filename}`)) {
        res.status(500).json({"message": "Server error"});
        return;
    }
    fs.writeFile(`./api/files/${filename}`, `${req.body.content}`, function (err) {
        if (err) {
            res.status(400).json({
                "message": "Please specify 'content' parameter"
            })
            // throw err;
        }
        res.status(200).json({"message": "File created successfully"});
    });
})

app.get('/api/files', (req, res) => {
    fs.readdir('./api/files', (err, files) => {
        if(files.length === 0) {
            res.status(500).json({
                "message": "Server error"
            });
            return;
        }
        if (err) {
            res.status(400).json({
                "message": "Client error"
            });
            return;
        }
        res.status(200).json({
            "message": "Success",
            "files": files
        });
    })
});

app.get('/api/files/:filename', (req, res) => {
    let uploadedDate = '';
    const extension = (req.params.filename).match(/[A-Za-z]+$/gm).join('');
    fs.stat(`./api/files/${req.params.filename}`,function (err, stat) {
            if (err) {
                console.log(err);
            } else {
                uploadedDate = stat.birthtime;
            }
        }
    );

    if(!extension) {
        res.status(500).json({
            "message": "Server error"
        });
        return;
    }
    fs.readFile(`./api/files/${req.params.filename}`, "utf8",
        function(error,data){
            if(error) {
                res.status(400).json({
                    "message": `No file with '${req.params.filename}' filename found`
                });
                return;
            }
            res.status(200).json({
                "message": "Success",
                "filename": req.params.filename,
                "content": data,
                "extension": extension,
                "uploadedDate": uploadedDate
            });
        });
});
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})